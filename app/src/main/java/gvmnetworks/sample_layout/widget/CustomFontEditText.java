package gvmnetworks.sample_layout.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by ahmad.syafii on 7/24/2017.
 */

public class CustomFontEditText extends AppCompatEditText {

    public CustomFontEditText(Context context) {
        super(context);

        CustomFontUtil.applyCustomFont(this, context, null);
    }

    public CustomFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtil.applyCustomFont(this, context, attrs);
    }

    public CustomFontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        CustomFontUtil.applyCustomFont(this, context, attrs);
    }
}
