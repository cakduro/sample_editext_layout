package gvmnetworks.sample_layout.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by ahmad.syafii on 6/9/2017.
 */

public class CustomFontButton extends AppCompatButton {

    public CustomFontButton(Context context) {
        super(context);

        CustomFontUtil.applyCustomFont(this, context, null);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtil.applyCustomFont(this, context, attrs);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        CustomFontUtil.applyCustomFont(this, context, attrs);
    }
}
